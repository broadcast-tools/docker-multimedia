# Docker Multimedia Container

Based on Alpine Linux (https://alpinelinux.org/)

## Packages included

  - FFmpeg (https://ffmpeg.org/)
  - MediaInfo (https://mediaarea.net/de/MediaInfo)
  - ImageMagick (https://imagemagick.org/)

## Example

```
docker run --rm -it -v <source-directory>:/data registry.gitlab.com/broadcast-tools/docker-multimedia /usr/bin/ffmpeg -i /data/<source-file> -c copy -map 0 -map_metadata 0 -movflags use_metadata_tags /data/<target-file>
```
